import java.io.File;
import java.util.Properties;

import org.apache.flink.core.fs.Path;
import org.apache.flink.formats.parquet.avro.ParquetAvroWriters;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.bucketassigners.DateTimeBucketAssigner;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.OnCheckpointRollingPolicy;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.types.Record;
import org.apache.log4j.BasicConfigurator;

import static org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.util.ClassUtil.classOf;

public class readData {

    public static void main(String[] args) throws Exception{
        BasicConfigurator.configure();

        // Setting the destination folder for part files
        final File folder = new File("C:\\dev\\todo");

        //Checkpoint interval - part files get saved on every checkpoint for Bulk Format
        long checkpointInterval = 60000;

        //Topic from which the stream is getting data
        String kafkaTopic = "rocket";

        //Setting properties for Kafka consumer
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        // only required for Kafka 0.8
        properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "test");

        //Creating environment for Flink job
        StreamExecutionEnvironment env = StreamExecutionEnvironment
                .getExecutionEnvironment();
        //env.setParallelism(1);
        env.setParallelism(1);
        env.enableCheckpointing(checkpointInterval);

        DataStream<User> stream = env.addSource(new FlinkKafkaConsumer<>(kafkaTopic,
                new UserDeserializationSchema(), properties));

        //Uncomment the below line to print the toString method of the class (User.java in this case)

        //stream.print();

        //Adding sink for saving files
         StreamingFileSink<User> sink = StreamingFileSink
                .<User>forBulkFormat(Path.fromLocalFile(folder), ParquetAvroWriters.forReflectRecord(User.class))
                 /*for ReflectRecord creates the ParquetWriterFactory for the given custom class
                    https://ci.apache.org/projects/flink/flink-docs-release-1.7/api/java/org/apache/flink/formats/parquet/avro/ParquetAvroWriters.html
                  */

                 //For Bulk Format the rolling pollicy is due to checkpoint
                .withRollingPolicy(OnCheckpointRollingPolicy.
                        <User, String>build())
                // .withBucketAssigner(new DateTimeBucketAssigner<User>("yyyy-MM-dd--HH"))
                 // Default optional BucketAssigner has been provided. Can be changed based on need
                 .build();
        stream.addSink(sink);


        env.execute();

    }
}
