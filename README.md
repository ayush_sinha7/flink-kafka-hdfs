## Kafka-Flink-HDFS write

The given program consumes JSON from Kafka, deserializes it to a POJO and writes it as Parquet onto a directory path.

---

## To Run the Program

1. Run zookeeper and kafka servers from terminal
2. Run the main function from **readData.java** in the code.
3. Start sending JSONs from a Kafka Producer
4. The Parquet files will be saved on the destination

---

## Producer Example for kafka-python
```
from kafka import KafkaProducer
import json, time
kafka_producer = KafkaProducer(bootstrap_servers=['localhost:9092'])
for i in range(10):
    data = {
        "user_id": "zaher", 
        "date": time.time()*1000,
        "type": "user_interaction"
    }
    future = kafka_producer.send('rocket', json.dumps(data).encode("utf-8"))
    record_metadata = future.get(timeout = 5)
    print(record_metadata)
    time.sleep(1)
```
---
## Sample output

| userId     | date            | type                |
| -----------|-----------------|---------------------|
| zaher      | 1587560871606   | user_interaction    |
| zaher      | 1587560871619   | user_interaction    |

---
*Some additional checkpointing settings can be made onto the program either in the yaml file or from the readData.java file itself