import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonSerialize
/*
    This creates a User class. Makes use of Lombok for constructor, getter, setter
 */
public class User {


    @JsonProperty("user_id")
    String userId;

    @JsonProperty("date")
    Long date;

    @JsonProperty("type")
    String type;

    //Modified toString method
    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                '}';
    }
}
