import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.io.IOException;

//This class is used to Deserialize the incoming JSON object from Kafka to POJO
public class UserDeserializationSchema implements
        DeserializationSchema<User> {
    static ObjectMapper objectMapper = new ObjectMapper()
            .registerModule(new JavaTimeModule());

    @Override
    public User deserialize(byte[] bytes) throws IOException {
        return objectMapper.readValue(bytes, User.class);
    }

    @Override
    public boolean isEndOfStream(User user) {
        return false;
    }

    @Override
    public TypeInformation<User> getProducedType() {
        return TypeInformation.of(User.class);
    }
}
